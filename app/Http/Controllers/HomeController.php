<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Home Controller
      |--------------------------------------------------------------------------

      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index() {

        $userType = \Auth::user()->userType;
        if ($userType === 'Admin') {
            return view('admin.home_admin');
        }
        if ($userType === 'user') {
            return view('subuser.home_user');
        }
    }

    public function changeUser() {
        $user = \App\User::find(\Auth::user()->id);
        if (\Auth::user()->userType == 'Admin') {
            $user->userType = "user";
        } elseif (\Auth::user()->userType == 'user') {
            $user->userType = "Admin";
        }
        $user->save();
        return \Redirect::to('\home');
    }

    public function ViewTransactions() {
        $cow = \App\Cow::where('tsk', '0')->orderBy('id', 'desc')->take(10)->get();
        $tabo_tabo = \App\TaboTabo::where('tsk', '0')->orderBy('id', 'desc')->take(10)->get();
        $outsource = \App\Outsource::where('tsk', '0')->orderBy('id', 'desc')->take(10)->get();
        $artcles = \App\Article::where('tsk', '0')->orderBy('name')->get();
        //dd($cow['article_resources']);
        return view('subuser.user_transactions')
                        ->with('cow', $cow)
                        ->with('tabo_tabos', $tabo_tabo)
                        ->with('outsources', $outsource)
                        ->with('articles', $artcles);
    }

    public function showStock() {
        $article = \App\Article_History::all();
        $articles = \App\Article::all();
        foreach ($article as $key => $art) {
            $qty1[$art['date']] = $art['date'];
        }
        //dd($article);
        return view('Admin.stock')
                        ->with('articles', $articles)
                        ->with('date', $qty1);
    }

    public function showPreferences() {
        $action = \Session::get('action');
        //dd($action);
        $article = \App\Article::all();
        if ($action == '') {
            $action = "editprofile";
        }

        return view('Admin.preference')
                        ->with('action', $action)
                        ->with('articles', $article);
    }

    public function ViewBaka() {
        $cow = \App\Cow::orderBy('id', 'desc')->paginate(5);
        $cow->setPath('http://localhost/wbms_final/public/baka');
        $tabotabo = \App\TaboTabo::orderBy('id', 'desc')->paginate(5);
        $tabotabo->setPath('http://localhost/wbms_final/public/baka');
        $article = \App\Article::all();
        // dd($mainparts);
        return view('subuser.baka_manage')
            ->with('article', $article)
            ->with('tabos', $tabotabo)
            ->with('cows', $cow);
    }

    public function ViewMeatshop() {
        //  dd($article);
        return view('Admin.admin_meatshop');
    }

    public function ViewVendor() {
        //  dd($article);
        $article = \App\Article::all();
        $vendor = \App\Outsource::all();
        //$article_view = \App\Article_Resource::where('outsource_id','=', $vendor->id)->get();
        return view('subuser.vendor_to_vendor')
                        ->with('article', $article)
                        //  ->with('article_view', $article_view)
                        ->with('vendor', $vendor);
    }

    public function ViewVendor_articleOrder($id) {
        //  dd($article);
        $articles = \App\Article_Resource::where('outsource_id', '=', $id)->get();
        //dd($article);
        return view('subuser.vendor_articleOrder')
                        ->with('articles', $articles);
    }

}
