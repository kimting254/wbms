<?php namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Support\Facades\Storage;
Use Validator;
use Illuminate\Http\Request;

class AdminController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function ShowProfile()
    {
        $action = "editprofile";
        \Session::put('action', $action);
        return view('Admin.preference')
            ->with('action', $action);
    }

    public function EditProfile()
    {
        $in = \Input::all();
        // dd($in);
        $user = \App\User::find(\Auth::user()->id);
        $user->name = $in['name'];
        $user->email = $in['email'];
        if ($in['new_password'] == '' || $in['old_password'] == '') {

        } else {
            if ($in['new_password'] == $in['old_password']) {
                $user->password = \Hash::make($in['new_password']);
            } else {
                \Flash::warning('Password doesnt match!');
                return \Redirect::back();
            }
        }
        $user->save();
        flash('Successfully change!');
        return \Redirect::back();
    }

    public function ShowThemes()
    {
        $action = "themes";
        \Session::put('action', $action);
        return view('Admin.preference')
            ->with('action', $action);
    }

    public function EditThemes()
    {
        $in = \Input::all();
        // dd($in);
        $user = \App\User::find(\Auth::user()->id);
        $user->labels = $in['label'];
        $user->panels = $in['panel'];
        $user->buttons = $in['button'];
        $user->bc = $in['bc'];
        $user->bi = $in['bi'];
        $user->opacity = $in['opa'];
        $target_dir = "assets/img/b";
        if ($in['pref'] == '0') {
            $user->backimage = "0";
        } else {
            $user->backimage = "1";
            $target_file = $target_dir . \Auth::user()->id . ".jpg";
            if (isset($_POST["submit"])) {
                $check = getimagesize($_FILES["file"]["tmp_name"]);
                if ($check == false) {
                    \Flash::warning('file is not an image');
                    return \Redirect::back();
                }
            } else {
                move_uploaded_file($_FILES["file"]["tmp_name"], $target_file);
                $user->backimage = "1";
            }
        }

        // Check file size
        $user->save();
        flash('successfully changed');
        $action = "themes";
        return \Redirect::to('/preferences')->with('action', $action);
    }

    public function ShowArticles()
    {
        $action = "articles";
        \Session::put('action', $action);
        $article = \App\Article::all();
        return view('Admin.preference')
            ->with('action', $action)
            ->with('articles', $article);
    }

    public function EditArticles($id)
    {
        $article = \App\Article::find($id);
        $article->name = \Input::get('article');
        $article->save();
        flash('Successfully change!');
        return \Redirect::back();
    }

    public function DeleteArticles($id)
    {
        $article = \App\Article::find($id);
        $article->delete();
        flash('Successfully delete!');
        return \Redirect::back();
    }

    public function AddArticles()
    {
        $article = new \App\Article;
        $article->name = \Input::get('article');
        $article->save();
        flash('Successfully added!');
        return \Redirect::back();
    }

    public function ShowUsers()
    {
        $action = "users";
        $user = \App\User::all();
        return view('Admin.preference')
            ->with('action', $action)
            ->with('users', $user);
    }

    public function EditUser($id)
    {
        $user = \App\User::find($id);
        $user->name = \Input::get('name');
        $user->email = \Input::get('email');
        $user->userType = \Input::get('userType');
        $user->save();
        flash('Successfully change!');
        return \Redirect::back();
    }

    public function DeleteUser($id)
    {
        $user = \App\User::find($id);
        if (\Auth::user()->id == $id) {
            $user->delete();
            \Flash::warning('You just delete your account. :(');
            return \Redirect::to('auth/login');
        }
        $user->delete();
        flash('Successfully delete!');
        return \Redirect::back();
    }

    public function ResetUser($id)
    {
        $user = \App\User::find($id);
        $newpass = \Hash::make('123');
        $user->password = $newpass;
        $user->save();
        flash('Successfully change!');
        return \Redirect::back();
    }

    public function AddUser()
    {
        $in = \Input::all();
        $check = $this->validator($in);
        if ($check->fails()) {
            \Flash::warning('*Unsuccessful, Check your inputs. - Email must be unique/required  - Password must match   - name is required');
            return \Redirect::back();
        }
        $user = new \App\User;
        $user->name = $in['name'];
        $user->email = $in['email'];
        $user->userType = $in['userType'];
        if ($user->userType = 'Admin') {
            $user->admin = '1';
        }
        $user->labels = 'default';
        $user->panels = 'default';
        $user->buttons = 'default';
        $user->bc = 'white';
        $user->bi = 'default';
        $user->backimage = '0';
        if ($in['password'] == $in['password_confirmation']) {
            $user->password = \Hash::make($in['password']);
        } else {
            \Flash::warning('Password Does Not Match!');
            return \Redirect::back();
        }
        $user->save();
        flash('Successfully added!');
        return \Redirect::back();
    }

    public function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }
}
