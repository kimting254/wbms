<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReceiptController extends Controller
{
    /*
      |--------------------------------------------------------------------------
      | Home Controller
      |--------------------------------------------------------------------------

      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function SaveOr($id)
    {
        $data = \Input::all();
        //dd($data);
        $order = \App\Order::find($id);
        $order->check = $data['checkno'];
        $order->bank = $data['bank'];
        $order->or = $data['or'];
        $order->date_of_check = $data['dateofcheck'];
        flash('Successfully Edited!');
        $order->save();
        return \Redirect::back();
    }

    public function ViewOR()
    {
        $pagination = \App\Order::where('status', '=', 'paid')->orderBy('or', 'desc')->paginate(8);
        $paginations = \App\Payment_History::where('balance', '>', '0.0')->orderBy('or', 'desc')->paginate(8);
        //$partial = \App\Payment_History::where('order','=',$pagination1->id)->get();
        // dd($orders);
        $pagination->setPath('http://localhost/wbms/public/or');
        $paginations->setPath('http://localhost/wbms/public/or');
        return view('Admin.customer_or.admin_OR')
            ->with('paginations', $paginations)
            ->with('order', $pagination);
    }

    public function ViewOrBreakdown($id)
    {
        $orderslist = \App\Orderlist::where('order_id', '=', $id)->get();
        $history = \App\Payment_History::where('order_id', '=', $id)->get();
        return view('Admin.customer_or.admin_or_breakdown')
            ->with('history', $history)
            ->with('data', $orderslist);
    }

    public function ViewDrBreakdown($id)
    {
        $orderslist = \App\Orderlist::where('order_id', '=', $id)->get();
        $history = \App\Payment_History::where('order_id', '=', $id)->get();
        //dd($orderslist);
        return view('Admin.customer_dr.admin_dr_breakdown')
            ->with('history', $history)
            ->with('data', $orderslist);
    }

    public function PostPayment($id)
    {
        $data = \Input::all();
         // dd($data);
        $order = \App\Order::find($id);
        $history = new \App\Payment_History;
        //$order->status = "paid";
        //$order->bank = $data["bank"];
        $order->or = $data['or'];
        $history->or = $data['or'];
        $order->amount_paid = $data['amount'];
        $history->amount_paid = $data['amount'];
        $order->date_of_check = $data['dateofcheck'];
        $history->date_of_check = $data['dateofcheck'];
        $count = count($data);
        $history->order_id = $order->id;
        if($count>4){
            $order->bank = $data['bank'];
            $history->bank = $data['bank'];
            $order->check = $data['checkno'];
            $history->check = $data['checkno'];
        }
        else{

        }
        //dd($post_payment->id);
        $order->balance = $order->balance - $order->amount_paid;
        $history->balance = $order->balance;
        //dd($order->balance);
        if($order->balance > 0.0){
            $order->status = 'partial';
        }
        elseif ($order->balance == 0.0){
            $order->status = 'paid';
        }
        //dd($order->status);
        $order->save();
        $history->save();
        flash('Payment Posted!');
        return \Redirect::to('/order_dr' . $id);
    }

    public function SaveDr($id)
    {
        $data = \Input::all();
        $order = \App\Order::find($id);
        $order->due_date = $data['due_date'];
        $order->dr = $data['dr'];
        $order->date = $data['date'];
        $order->status = $data['status'];
        $order->total_due = $data['amount'];
        flash('Successfully Edited!');
        $order->save();
        return \Redirect::back();
    }

    public function DeleteDr($id)
    {
        $order = \App\Order::find($id);
        foreach ($order->orderlists as $list) {
            $list->delete();
        }
        $order->delete();
        flash('DR deleted!');
        return \Redirect::back();
    }

    public function DeleteOr($id)
    {
        $order = \App\Order::find($id);
        foreach ($order->orderlists as $list) {
            $list->delete();
        }
        $order->delete();
        flash('OR deleted!');
        return \Redirect::back();
    }


    public function ViewDR()
    {
        $now = (int)strtotime(date("m/d/y", time()));
        $pagination = \App\Order::where('type', '=', 'delivery')->orderBy('id', 'desc')->paginate(8);
        //  dd($orders);
        $pagination->setPath('http://localhost/wbms/public/dr');

        return view('Admin.customer_dr.admin_DR')
            ->with('order', $pagination)
            ->with('now', $now);
    }

    public function Post_vendorPayment($id){
        $pay = \Input::all();
        //dd($id);
        $post_payment = \App\Outsource::find($id);
        $history = new \App\Payment_History;
        //dd($post_payment->id);
        $post_payment->amount_paid = $pay['amount'];
        $history->amount_paid = $pay['amount'];
        $post_payment->or = $pay['or'];
        $history->or = $pay['or'];
        $post_payment->date_of_check = $pay['dateofpayment'];
        $history->date_of_check = $pay['dateofpayment'];
        $history->outsource_i = $post_payment->id;
        $count = count($pay);
        if($count>4){
            $post_payment->Bank = $pay['bank'];
            $history->bank = $pay['bank'];
            $post_payment->Check = $pay['checkno'];
            $history->check = $pay['checkno'];
        }
        else{

        }
        //dd($post_payment->id);
        $post_payment->balance = $post_payment->balance - $post_payment->amount_paid;
        $history->balance = $post_payment->balance;
        //dd($post_payment->balance);
        if($post_payment->balance > 0){
            $post_payment->status = 'partial';
        }
        elseif ($post_payment->balance = 0){
            $post_payment->status = 'paid';
        }
        $post_payment->save();
        $history->save();
        flash('Payment Posted!');
        return \Redirect::back();
    }
}
