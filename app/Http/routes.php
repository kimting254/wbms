<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */
/*Routes for bringing up the home & general pages*/
Route::get('/', 'HomeController@index');
Route::get('home', 'HomeController@index');
//Route::get('auth/home', 'HomeController@index');
Route::get('/transactions', 'HomeController@ViewTransactions');
Route::get('/baka', 'HomeController@ViewBaka');
Route::get('/meatshop', 'HomeController@ViewMeatshop');
Route::get('/showStock','HomeController@showStock');
Route::get('/preferences','HomeController@showPreferences');
Route::get('/change_user','HomeController@changeUser');

/*Routes for OR & DR pointed to ReceiptController*/
Route::get('/or', 'ReceiptController@ViewOR');
Route::post('/or', 'ReceiptController@ViewOR');
Route::post('/dr', 'ReceiptController@ViewDR');
Route::get('/dr', 'ReceiptController@ViewDR');
Route::get('/order_or{id}', 'ReceiptController@ViewOrBreakdown');
Route::get('/order_dr{id}', 'ReceiptController@ViewDRBreakdown');
Route::post('/save_or/{id}', 'ReceiptController@SaveOr');
Route::post('/save_dr/{id}', 'ReceiptController@SaveDr');
Route::post('/postpayment/{id}', 'ReceiptController@PostPayment');
Route::post('/delete_dr/{id}', 'ReceiptController@DeleteDr');
Route::post('/delete_or/{id}', 'ReceiptController@DeleteOr');
Route::post('/post_vendorPayment{id}','ReceiptController@Post_vendorPayment');

/*Routes for managing resources pointed to resourceController*/
Route::post('/delete_outsource/{id}', 'ResourceController@DeleteOutsource');
Route::post('/edit_outsource/{id}', 'ResourceController@EditOutsource');
Route::post('/delete_own/{id}', 'ResourceController@DeleteOwn');
Route::post('/edit_own/{id}', 'ResourceController@EditOwn');
Route::post('/delete_tabo/{id}', 'ResourceController@DeleteTabo');
Route::post('/edit_tabo/{id}', 'ResourceController@EditTabo');
Route::post('/save_baka/{id}', 'ResourceController@SaveBaka');
Route::post('/delete_baka/{id}', 'ResourceController@DeleteBaka');
Route::get('/resource_breakdown{id}', 'ResourceController@ResourceBreakdown');
Route::post('/view_tabo_tabo', 'ResourceController@ViewResourcesTabo');
Route::get('/view_tabo_tabo', 'ResourceController@ViewResourcesTabo');
Route::get('/viewTaboBr_{id}', 'ResourceController@ViewTaboBr');
Route::post('/addExpense', 'ResourceController@AddExpense');
Route::post('/addArticles', 'ResourceController@AddArticle');

Route::get('/own', 'ResourceController@ViewFromOwnFarm');
Route::get('/outsource', 'ResourceController@ViewFromOutsource');
Route::post('/addTabo', 'ResourceController@AddTabo');
Route::post('/addVendorOrder', 'ResourceController@AddDeliveryTabo');
Route::get('/cow_info{id}','ResourceController@CowInfo');
Route::post('/addBaka', 'ResourceController@AddBaka');
Route::post('/addParts{id}', 'ResourceController@AddParts');
Route::post('/addCuts{id}','ResourceController@AddCuts');
Route::get('/outsource_reports{id}', 'ResourceController@ViewReports');

//new--Cindy
Route::get('/viewParts{id}', 'ResourceController@ViewParts');
Route::post('/delete_part{id}', 'ResourceController@DeleteParts');
Route::post('/delete_cuts{id}', 'ResourceController@DeleteCuts');
Route::post('/editPart{id}', 'ResourceController@EditPart');
Route::post('/editCut{id}', 'ResourceController@EditCut');
Route::post('/addTaboArticles{id}', 'ResourceController@AddTaboArticles');
Route::get('/viewTaboArticles{id}', 'ResourceController@ViewTaboArticles');
Route::post('/deleteTaboArticle{id}', 'ResourceController@DeleteTaboArticle');
Route::post('/editTaboArticle{id}', 'ResourceController@EditTaboArticle');

/*Routes for Exporting Records*/
Route::post('/exportOR','ExportController@ExportOR');
Route::post('/exportDR','ExportController@ExportDR');
Route::post('/exportTabo','ExportController@ExportTabo');
Route::post('/exportOutSource','ExportController@ExportOutsource');
Route::post('/exportOwn','ExportController@ExportOwn');
Route::get('/printCowReport{id}', 'ExportController@printCowReport');
Route::get('/printTaboReport{id}', 'ExportController@printTaboReport');


/* Admin Controller for preferences */
Route::get('/profile','AdminController@ShowProfile');
Route::post('/editProfile','AdminController@EditProfile');
Route::get('/themes{id}','AdminController@ShowThemes');
Route::post('/editThemes','AdminController@EditThemes');

//Articles
Route::get('/articles','AdminController@ShowArticles');
Route::post('/edit_article{id}','AdminController@EditArticles');
Route::post('/delete_article{id}','AdminController@DeleteArticles');
Route::post('/add_article','AdminController@AddArticles');

//users
Route::get('/users','AdminController@ShowUsers');
Route::post('/edit_user{id}','AdminController@EditUser');
Route::post('/delete_user{id}','AdminController@DeleteUser');
Route::post('/add_user','AdminController@AddUser');
Route::post('/reset_user{id}','AdminController@ResetUser');

/* routes for orders */
Route::post('/addOrder','OrderController@AddOrder');
Route::post('/addPurchase','OrderController@AddPurchase');
Route::post('/add_outsourceOrder','OrderController@Add_outsourceOrder');
Route::get('/vendor', 'HomeController@ViewVendor');
Route::get('/vendor_articleOrder{id}', 'HomeController@ViewVendor_articleOrder');


Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',

]);

