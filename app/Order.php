<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model {

    //
    public $timestamps = false;
    public function customer() {
        return $this->belongsTo('App\Customer');
    }

    public function orderlists() {
        return $this->hasMany('App\Orderlist');
    }

    public function payment_history(){
        return $this->hasMany('App\Payment_History');
    }
}
