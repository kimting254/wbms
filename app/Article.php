<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{

    //
    public function article_resources()
    {
        return $this->hasMany('App\Article_Resource');
    }

    public function orderlists()
    {
        return $this->hasMany('App\Orderlist');
    }
    public function main_parts(){
        return $this->hasMany('App\MainParts');
    }

    public function article_histories()
    {
        return $this->hasMany('App\Article_History');
    }
}
