@extends('app')

@section('content')
    <style>
        #arrow1 {
            font-weight: bold;
        }

        td, th {
            padding: 2px;
        }
        #arrow{
            visibility:hidden;
        }
    </style>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="panel panel-{{Auth::user()->panels}}">
                    <div class="panel-heading" style="height: 80px;">
                        <h3 class="pull-left" style="margin-top: 20px"> Own Cow </h3>
                        <label class="pull-right">{!! $cows->render() !!}</label>
                    </div>
                    <div class="panel-body">
                        <table class="table-responsive col-lg-12 col-md-12 ">
                            <thead>
                            <th>Date Added</th>
                            <th>Body #</th>
                            <th>Color</th>
                            <th>Gender</th>
                            <th>LW</th>
                            <th>SW</th>
                            <th>CW</th>
                            <th>MW</th>
                            <th>Action</th>
                            </thead>
                            <tbody>
                            @foreach($cows as $cow)
                                <tr>
                                    <td>{{ $cow->date}}</td>
<!--                                    <td><a class="btn " data-toggle="modal"
                                           data-target="#viewpartsModal{{ $cow->id}}">{{ $cow->id}}</a>
                                         View Button Modal
                                    </td>-->
                                    <td><a href="./viewParts{{ $cow->id }}">{{ $cow->id }}</a></td>
                                    <td>{{ $cow->color}}</td>
                                    <td>{{ $cow->sex}}</td>
                                    <td>{{ $cow->lw}}</td>
                                    <td>{{ $cow->sw}}</td>
                                    <td>{{ $cow->cw}}</td>
                                    <td>{{ $cow->mw}}</td>
                                    <td>
                                        <button class="btn btn-{{Auth::user()->buttons}}" data-toggle="modal"
                                                data-target="#addParts{{ $cow->id}}">
                                            Add Parts
                                        </button>
                                        <button class="btn btn-{{Auth::user()->buttons}}" data-toggle="modal"
                                                data-target="#addCuts{{ $cow->id}}">
                                            Add Cuts
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <div class="pull-right" style="padding-top: 20px;"><a>
                                <button class="btn btn-{{Auth::user()->buttons}}" data-toggle="modal" data-target="#addBaka">Add
                                    Slaughtered Baka
                                </button>
                            </a></div>
                    </div>
                </div>
            </div>
            
            <!--Tabo-Tabo-->
            
            <div class="col-md-12 col-lg-12">
                <div class="panel panel-{{Auth::user()->panels}}">
                    <div class="panel-heading" style="height: 80px;">
                        <h3 class="pull-left" style="margin-top: 20px"> Tabo - Tabo </h3>
                        <label class="pull-right">{!! $tabos->render() !!}</label>
                    </div>
                    <div class="panel-body">
                        <table class="table-responsive col-lg-12 col-md-12 ">
                            <thead>
                            <th>Date of Purchase</th>
                            <th>Body #</th>
                            <th>Color</th>
                            <th>Gender</th>
                            <th>LW</th>
                            <th>SW</th>
                            <th>CW</th>
                            <th>MW</th>
                            <th>Price</th>
                            <th>Area</th>
                            <th>Owner</th>
                            <th>Action</th>
                            </thead>
                            <tbody>
                            @foreach($tabos as $tabo)
                                <tr>
                                    <td>{{ $tabo->date_of_purchase}}</td>
                                    <td><a href="./viewTaboArticles{{ $tabo->id }}">{{ $tabo->id }}</a></td>
                                    <td>{{ $tabo->color}}</td>
                                    <td>{{ $tabo->sex}}</td>
                                    <td>{{ $tabo->lw}}</td>
                                    <td>{{ $tabo->sw}}</td>
                                    <td>{{ $tabo->cw}}</td>
                                    <td>{{ $tabo->mw}}</td>
                                    <td>{{ $tabo->price}}</td>
                                    <td>{{ $tabo->area}}</td>
                                    <td>{{ $tabo->owner}}</td>
                                    <td>
                                        <button class="btn btn-{{Auth::user()->buttons}}" data-toggle="modal"
                                                data-target="#addArticle{{ $tabo->id}}">
                                            Add Articles
                                        </button>                                       
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>                        
                    </div>
                </div>
            </div>
            
           
    <!-- Add Baka-->
    <div class="modal fade" id="addBaka" role="dialog">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">New Slaughtered Baka</h4>
                </div>
                <form type="hidden" method="post" action="./addBaka" id="form1"/>
                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

                <div class="container col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group col-lg-6 col-md-6 col-sm-6">
                        <label for="">Date Added</label>
                        <input type='text' name="date" class="form-control"
                               id='datetimepicker4'/>
                    </div>
                    <div class="form-group col-lg-6 col-md-6 col-sm-6">
                        <label for="color">Color:</label>
                        <input type="text" name="color" value="" placeholder=""
                               class="form-control">
                    </div>
                    <div class="form-group col-lg-6 col-md-6 col-sm-6">
                        <label for="gender">Gender</label>
                        <select name="gender" value="" placeholder="" class="form-control">
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </select>
                    </div>

                    <div class="form-group col-lg-6 col-md-6 col-sm-6">
                        <label for="lw">Live Wt:</label>
                        <input type="text" value="" placeholder="" class="form-control"
                               name="lw" min="1">
                    </div>
                    <div class="form-group col-lg-6 col-md-6 col-sm-6">
                        <label for="sw">Slaughter Wt:</label>
                        <input type="text" value="" placeholder="" class="form-control"
                               name="sw" min="1">
                    </div>
                    <div class="form-group col-lg-6 col-md-6 col-sm-6">
                        <label for="cw">Carcass Wt:</label>
                        <input type="text" value="" placeholder="" class="form-control"
                               name="cw" min="1">
                    </div>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-danger"><span
                                class="glyphicon glyphicon-remove"></span>
                        Cancel
                    </button>
                    <button type="submit" class="btn btn-{{Auth::user()->buttons}}"><span
                                class="glyphicon glyphicon-save"></span> Save
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>   

    @foreach($cows as $cow)
        <div class="modal fade" id="viewpartsModal{{ $cow->id}}" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close"
                                data-dismiss="modal">&times;</button>
                        <h3 class="modal-title">Parts - Body # {{ $cow->id}}</h3>
                    </div>
                    <table class="table-responsive col-lg-12 col-md-12 table-hover">
                        <thead>
                        <th>Articles</th>
                        <th>Quantity ( Kls )</th>
                        </thead>
                        <tbody>
                        @foreach ($cow['main_parts'] as $part)
                            <tr>
                                <td>{{ $part->article}}</td>
                                <td>{{ $part->quantity}}</td>
                            </tr>
                        @endforeach
                        <tr style="border-top:solid 2px;">
                            <td><b>Total</b></td>
                            <td><b>{{$cow->mw}} Kls.</b></td>
                        </tr>
                        </tbody>
                    </table>

                    <div class="modal-footer">
                        <button type="submit" data-dismiss="modal"
                                class="btn btn-{{Auth::user()->buttons}}"><span
                                    class="glyphicon glyphicon-check"></span> Ok
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="addParts{{$cow->id}}" role="dialog">
            <div class="modal-dialog ">
                <div class="modal-content" >
                    <div class="modal-header">
                        <button type="button" class="close"
                                data-dismiss="modal">&times;</button>
                        <h2 class="modal-title">Add Parts - Body # {{ $cow->id}}</h2>
                    </div>
                    <form method="post" action="./addParts{{$cow->id}}">
                        <input type="hidden" name="_token"
                               value="{{csrf_token() }}"/>
                        <input type="hidden" name="cow_id" id="cow_id{{ $cow->id }}"
                               value="{{ $cow->id }}"/>
                        <div class="modal-body">
                            <div id="parentDiv{{ $cow->id }}"
                                 class="container col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group col-lg-7 col-md-7 col-sm-7">
                                    <label for="part">Parts</label>
                                    <select name="part{{$cow->id}}0" class="form-control" size = "1">
                                        @foreach($article as $art)
                                            <option>{{ $art->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-3 col-sm-3">
                                    <label for="Qty">Qty (kg):</label>
                                    <input type="text" name="qty{{ $cow->id}}0" value=""
                                           placeholder="in kg" class="form-control"
                                           id="Qty">
                                </div>
                                <script type="text/javascript">
                                    var i = 0;
                                    function AddData{{$cow->id}}() {
                                        i++;
                                        $('<div class="form-group col-lg-7 col-md-7 col-sm-7"><select name="part'+{{$cow->id}}+ i +'" class="form-control" size = "1"> @foreach($article as $art)<option>{{ $art->name }}</option>@endforeach</select></div> <div class="form-group col-lg-3 col-md-3 col-sm-3"><input type="text" name="qty' + {{$cow->id}} +i + '" value="" placeholder="in kg" class="form-control" min="1"></div> ').appendTo("#parentDiv" + {{$cow->id}});
                                    }
                                </script>
                            </div>
                            &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;<label
                                    onclick="AddData{{ $cow->id }}()"
                                    class="label label-info"
                                    style="font-size: medium"><span
                                        class="glyphicon glyphicon-plus"></span> Add
                                Part
                            </label>
                        </div>
                        <div class="modal-footer">
                            <button data-dismiss="modal"
                                    class="btn btn-danger"><span
                                        class="glyphicon glyphicon-remove"></span>
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-{{Auth::user()->buttons}}"><span
                                        class="glyphicon glyphicon-save"></span>
                                Save
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    
     <div class="modal fade" id="addCuts{{$cow->id}}" role="dialog">
            <div class="modal-dialog ">
                <div class="modal-content modal-lg" >
                    <div class="modal-header">
                        <button type="button" class="close"
                                data-dismiss="modal">&times;</button>
                        <h2 class="modal-title">Add Cuts - Body # {{ $cow->id}}</h2>
                    </div>
                    <form method="post" action="./addCuts{{$cow->id}}">
                        <input type="hidden" name="_token"
                               value="{{csrf_token() }}"/>
                        <input type="hidden" name="cow_id" id="cow_id{{ $cow->id }}"
                               value="{{ $cow->id }}"/>
                        <div class="modal-body">
                            <div id="cutDiv{{ $cow->id }}"
                                 class="container col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group col-lg-5 col-md-5 col-sm-5">
                                    <label for="cuts">Articles</label>
                                    <select name="part{{$cow->id}}0" class="form-control" size = "1">
                                        @foreach($article as $art)
                                            <option>{{ $art->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-3 col-sm-3">
                                    <label for="Qty">Qty (kg):</label>
                                    <input type="text" name="qty{{ $cow->id}}0" value=""
                                           placeholder="in kg" class="form-control"
                                           id="Qty">
                                </div>
                                <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                    <label for="type">Type</label>
                                    <select name="type{{$cow->id}}0" class="form-control" size = "1">                                       
                                            <option>Choice Cuts</option>  
                                            <option>Special Cuts</option>
                                            <option>Other Cuts</option>
                                    </select>
                                </div>
                                <script type="text/javascript">
                                   
                                    var j = 0;                                    
                                    function AddData1{{ $cow->id}}(){
                                        j++;
                                        $('<div class="form-group col-lg-5 col-md-5 col-sm-5"><select name="part'+{{$cow->id}}+ j +'" class="form-control" size = "1"> @foreach($article as $art)<option>{{ $art->name }}</option>@endforeach</select></div> <div class="form-group col-lg-3 col-md-3 col-sm-3"><input type="text" name="qty' + {{$cow->id}} + j + '" value="" placeholder="in kg" class="form-control" min="1"></div><div class="form-group col-lg-4 col-md-4 col-sm-4"><select name="type'+{{$cow->id}}+ j +'" class="form-control" size = "1"><option>Choice Cuts</option><option>Special Cuts</option><option>Other Cuts</option></select></div>').appendTo("#cutDiv" + {{$cow->id}});
                                         }
                                    
                                </script>
                            </div>
                            &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;<label
                                    onclick="AddData1{{ $cow->id }}()"
                                    class="label label-info"
                                    style="font-size: medium"><span
                                        class="glyphicon glyphicon-plus"></span> Add
                                Cut
                            </label>
                        </div>
                        <div class="modal-footer">
                            <button data-dismiss="modal"
                                    class="btn btn-danger"><span
                                        class="glyphicon glyphicon-remove"></span>
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-{{Auth::user()->buttons}}"><span
                                        class="glyphicon glyphicon-save"></span>
                                Save
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    @endforeach
    
    @foreach($tabos as $tabo)
    <div class="modal fade" id="addArticle{{$tabo->id}}" role="dialog">
            <div class="modal-dialog ">
                <div class="modal-content" >
                    <div class="modal-header">
                        <button type="button" class="close"
                                data-dismiss="modal">&times;</button>
                        <h2 class="modal-title">Add Articles - Body # {{ $tabo->id}}</h2>
                    </div>
                    <form method="post" action="./addTaboArticles{{$tabo->id}}">
                        <input type="hidden" name="_token"
                               value="{{csrf_token() }}"/>
                        <input type="hidden" name="tabo_id" id="tabo_id{{ $tabo->id }}"
                               value="{{ $tabo->id }}"/>
                        <div class="modal-body">
                            <div id="TaboDiv{{ $tabo->id }}"
                                 class="container col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group col-lg-7 col-md-7 col-sm-7">
                                    <label for="article">Articles</label>
                                    <select name="article{{$tabo->id}}0" class="form-control" size = "1">
                                        @foreach($article as $art)
                                            <option>{{ $art->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-3 col-sm-3">
                                    <label for="Qty">Qty (kg):</label>
                                    <input type="text" name="qty{{ $tabo->id}}0" value=""
                                           placeholder="in kg" class="form-control"
                                           id="Qty">
                                </div>
                                <script type="text/javascript">
                                    var k = 0;
                                    function AddData3{{$tabo->id}}() {
                                        k++;
                                        $('<div class="form-group col-lg-7 col-md-7 col-sm-7"><select name="article'+{{$tabo->id}}+ k +'" class="form-control" size = "1"> @foreach($article as $art)<option>{{ $art->name }}</option>@endforeach</select></div> <div class="form-group col-lg-3 col-md-3 col-sm-3"><input type="text" name="qty' + {{$tabo->id}} + k + '" value="" placeholder="kg" class="form-control" min="1"></div> ').appendTo("#TaboDiv" + {{$tabo->id}});
                                    }
                                </script>
                            </div>
                            &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;<label
                                    onclick="AddData3{{ $tabo->id }}()"
                                    class="label label-info"
                                    style="font-size: medium"><span
                                        class="glyphicon glyphicon-plus"></span> Add
                                Article
                            </label>
                        </div>
                        <div class="modal-footer">
                            <button data-dismiss="modal"
                                    class="btn btn-danger"><span
                                        class="glyphicon glyphicon-remove"></span>
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-{{Auth::user()->buttons}}"><span
                                        class="glyphicon glyphicon-save"></span>
                                Save
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    @endforeach
    <script type="text/javascript">
        document.getElementById("arrow1").innerHTML = "  Add Resource";
        $(document).ready(function () {
            $(function () {
                $('#datetimepicker4').datepicker();
            });
        });
        function myFunction() {

            var val = document.getElementById("mySelect").value;
            if (val === "None") {
                document.getElementById("myNumber").disabled = true;
            }
            else {
                document.getElementById("myNumber").disabled = false;
            }

        }
    </script>

@endsection