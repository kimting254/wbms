@extends('app')

@section('content')
<style>
    #arrow0 {
        font-weight: bold;
    }

    .table td {
        text-align: left;
        padding: 2px;
    }

    .panel-body {
        padding: 5px;
    }
    .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
        padding: 2px;
    }
    .jumbotron.vertical-center {
        margin-bottom: 0; /* Remove the default bottom margin of .jumbotron */
    }
    label.pull-right{
        margin-bottom: -18px;
        margin-top: -14px;
    }     
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-lg-8 col-sm-8 col-lg-offset-2">
            <div class="panel panel-{{Auth::user()->panels}}">
                <div class="panel-body" style="background-image: url('assets/img/.jpg')">
                    <div vlass = "well" style="text-align: center"><span class="glyphicon glyphicon-minus-sign"></span> Date :
                        <b>{{$cow['date']}}</b> &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp; <span
                            class="glyphicon glyphicon-minus-sign"></span> Body# : <b>{{$cow['id']}}</b> &nbsp;&nbsp;
                        &nbsp; &nbsp;&nbsp; &nbsp;<span class="glyphicon glyphicon-minus-sign"></span> LW :
                        <b>{{$cow['lw']}}</b> Kls. &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; <span
                            class="glyphicon glyphicon-minus-sign"></span> SW : <b>{{$cow['sw']}}</b> Kls.
                        &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp; <span class="glyphicon glyphicon-minus-sign"></span>
                        MW : <b>{{$cow['mw']}}</b> Kls.&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp; <span
                            class="glyphicon glyphicon-minus-sign"></span> <b>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8 col-lg-offset-2" >
            <div>
                <div class="panel panel-{{Auth::user()->panels}}">
                    <div class="panel-heading"> Own Cow </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-hover col-lg-12 col-md-12 col-xs-12">
                                <thead style="border-bottom:solid 2px;">
                                <th></th>
                                <th>Article Name</th>
                                <th>Quantity (Kg)</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><b>Base Parts</b></td>
                                        <td></td>
                                        <td></td>
                                        <td><label class="pull-right">{!! $mainparts->render() !!}</label></td>                                    
                                    </tr>
                                    @foreach($mainparts as $part)
                                    <tr>                               
                                        <td></td>
                                        <td>{{ $part->article }}</td>
                                        <td>{{ $part->quantity}}</td>
                                        <td>
                                            <a data-toggle="modal" data-target="#editPart{{$part['article_id']}}"><span class="glyphicon glyphicon-edit"></span></a>|
                                            <a data-toggle="modal" data-target="#deletePart{{$part['article_id']}}"><span class="glyphicon glyphicon-trash"></span></a>    
                                        </td>                                
                                    </tr>   
                                    @endforeach

                                    <tr style="border-top:solid 2px;">
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr> 
                                    
                                    <tr>
                                        <td><b>Choice Cuts</b></td>
                                        <td></td>
                                        <td></td>
                                        <td><label class="pull-right">{!! $choice_cuts->render() !!}</label></td>                                    
                                    </tr>
                                    @foreach($choice_cuts as $list)
                                    <!--@if($list['type']=='Choice Cuts')-->
                                    <tr>
                                        <td></td>
                                        <td>{{$list['article']['name']}}</td>

                                        <td> {{$list['quantity']}}</td>
                                        <td>
                                            <a data-toggle="modal" data-target="#editCut{{$list['article_id']}}"><span class="glyphicon glyphicon-edit"></span></a>|
                                            <a data-toggle="modal" data-target="#deleteCut{{$list['article_id']}}"><span class="glyphicon glyphicon-trash"></span></a>
                                        </td>                                           
                                    </tr>
                                    <!--@endif-->
                                    @endforeach
                                    <tr style="border-top:solid 2px;">
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>

                                    <tr>
                                        <td><b>Special Cuts</b></td>
                                        <td></td>
                                        <td></td>
                                        <td><label class="pull-right">{!! $special_cuts->render() !!}</label></td>                                    
                                    </tr>
                                    @foreach($special_cuts as $list)
                                    <!--@if($list['type']=='Special Cuts')-->
                                    <tr>
                                        <td></td>
                                        <td>{{$list['article']['name']}}</td>

                                        <td>{{$list['quantity']}}</td>
                                        <td>
                                            <a data-toggle="modal" data-target="#editCut{{$list['article_id']}}"><span class="glyphicon glyphicon-edit"></span></a> | 
                                            <a data-toggle="modal" data-target="#deleteCut{{$list['article_id']}}"><span class="glyphicon glyphicon-trash"></span></a>
                                        </td>                                            
                                    </tr>
                                    <!--@endif-->
                                    @endforeach
                                    <tr style="border-top:solid 2px;">
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>   
                                    </tr>

                                    <tr>
                                        <td><b>Other Cuts</b></td>
                                        <td></td>
                                        <td></td>
                                        <td><label class="pull-right">{!! $other_cuts->render() !!}</label></td>                                    
                                    </tr>
                                    @foreach($other_cuts as $list)
                                    <tr>
                                        <td></td>
                                        <td>{{$list['article']['name']}}</td>

                                        <td>{{ $list['quantity'] }} </td>
                                        <td> 
                                            <a  data-toggle="modal" data-target="#editCut{{$list['article_id']}}"><span class="glyphicon glyphicon-edit"></span></a> | 
                                            <a  data-toggle="modal" data-target="#deleteCut{{$list['article_id']}}"><span class="glyphicon glyphicon-trash"></span></a>
                                        </td>                                            
                                    </tr>
                                    @endforeach

                                <tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        @foreach($mainparts as $part)
        <div class="modal fade" id="deletePart{{$part['article_id']}}" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close"
                                data-dismiss="modal">&times;</button>
                        <h2 class="modal-title">Delete {{$part['article']}}</h2>
                    </div>
                    <div class="modal-body">
                        <form type="hidden" method="post"
                              action="./delete_part{{$part['article_id']}}" id="form1"/>
                        <input type="hidden" name="_token"
                               value="{{{ csrf_token() }}}"/>
                        <input type="hidden" name="cow_id"
                               value="{{$part['cow_id']}}"/>
                        <input type="hidden" name="article_id"
                               value="{{$part['article_id']}}"/>
                        <div class="container col-lg-12  col-md-12">
                            <h5> Are you sure you want to delete this part?</h5>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-danger"><span
                                class="glyphicon glyphicon-remove"></span> Cancel
                        </button>
                        <button type="submit" class="btn btn-{{Auth::user()->buttons}}"><span
                                class="glyphicon glyphicon-ok"></span>
                            Confirm
                        </button>
                    </div>
                </div>
                </form>
            </div>
        </div>

        <div class="modal fade center" id="editPart{{$part['article_id']}}" role="dialog">
            <div class="modal-dialog ">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close"
                                data-dismiss="modal">&times;</button>
                        <h2 class="modal-title">Edit {{ $part['article']}}</h2>
                    </div>
                    <form method="post" action="./editPart{{ $part['article_id']}}">
                        <input type="hidden" name="_token"
                               value="{{csrf_token() }}"/>
                        <input type="hidden" name="cow_id" id="cow_id"
                               value="{{ $part['cow_id'] }}"/>                        
                        <input type="hidden" name="old_qty" id="cow_id"
                               value="{{ $part['quantity'] }}"/>
                        <div class="modal-body">
                            <div id="parentDiv"
                                 class="container col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group col-lg-7 col-md-7 col-sm-7">
                                    <label for="part">Parts</label>
                                    <select name="part" class="form-control" size = "1">    
                                        <option>{{ $part['article'] }}</option>
                                        @foreach($articles as $art)
                                        <option >{{ $art->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-3 col-sm-3">
                                    <label for="Qty">Qty (kg):</label>
                                    <input type="text" name="qty" value="{{ $part['quantity']}}"
                                           placeholder="in kg" class="form-control"
                                           id="Qty">
                                </div>
                            </div>                           
                        </div>
                        <div class="modal-footer">
                            <button data-dismiss="modal"
                                    class="btn btn-danger"><span
                                    class="glyphicon glyphicon-remove"></span>
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-{{Auth::user()->buttons}}"><span
                                    class="glyphicon glyphicon-save"></span>
                                Save
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
        @endforeach

        @foreach($lists as $list)
        
        <div class="modal fade" id="deleteCut{{$list['article_id']}}" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close"
                                data-dismiss="modal">&times;</button>
                        <h2 class="modal-title">Delete {{$list['article']['name']}}</h2>
                    </div>
                    <div class="modal-body">
                        <form type="hidden" method="post"
                              action="./delete_cuts{{$list['article_id']}}" id="form1"/>
                        <input type="hidden" name="_token"
                               value="{{{ csrf_token() }}}"/>
                        <input type="hidden" name="cow_id"
                               value="{{$cow['id']}}"/>
                        <input type="hidden" name="article_id"
                               value="{{$list['article_id']}}"/>
                        <div class="container col-lg-12  col-md-12">
                            <h5> Are you sure you want to delete this part?</h5>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-danger"><span
                                class="glyphicon glyphicon-remove"></span> Cancel
                        </button>
                        <button type="submit" class="btn btn-{{Auth::user()->buttons}}"><span
                                class="glyphicon glyphicon-ok"></span>
                            Confirm
                        </button>
                    </div>
                </div>
                </form>
            </div>
        </div>    
        
        <div class="modal fade" id="editCut{{$list['article_id']}}" role="dialog">
            <div class="modal-dialog ">
                <div class="modal-content modal-lg" >
                    <div class="modal-header">
                        <button type="button" class="close"
                                data-dismiss="modal">&times;</button>
                        <h2 class="modal-title">Edit {{ $list['article']['name']}}</h2>
                    </div>
                    <form method="post" action="./editCut{{$list['article_id']}}">
                        <input type="hidden" name="_token"
                               value="{{csrf_token() }}"/>
                        <input type="hidden" name="cow_id" id="cow_id"
                               value="{{ $cow['id'] }}"/>
                        <input type="hidden" name="old_qty" id="cow_id"
                               value="{{ $list['quantity'] }}"/>
                        <input type="hidden" name="old_type" id="cow_id"
                               value="{{ $list['type'] }}"/>
                        <div class="modal-body">
                            <div id="cutDiv"
                                 class="container col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group col-lg-5 col-md-5 col-sm-5">
                                    <label for="article">Articles</label>
                                    <select name="article" class="form-control" size = "1">
                                        <option>{{ $list['article']['name'] }}</option>
                                        @foreach($articles as $art)
                                            <option>{{ $art['name'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-3 col-sm-3">
                                    <label for="qty">Qty (kg):</label>
                                    <input type="text" name="qty" value="{{ $list['quantity'] }}"
                                           placeholder="in kg" class="form-control"
                                           id="qty">
                                </div>
                                <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                    <label for="type">Type</label>
                                    <select name="type" class="form-control" size = "1"> 
                                            <option>{{ $list['type'] }}</option>
                                            <option>Choice Cuts</option>  
                                            <option>Special Cuts</option>
                                            <option>Other Cuts</option>
                                    </select>
                                </div>                                
                            </div>                            
                        </div>
                        <div class="modal-footer">
                            <button data-dismiss="modal"
                                    class="btn btn-danger"><span
                                        class="glyphicon glyphicon-remove"></span>
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-{{Auth::user()->buttons}}"><span
                                        class="glyphicon glyphicon-save"></span>
                                Save
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
      
        @endforeach

        <script>
                    document.getElementById("arrow0").innerHTML = "  Add a Resource";
                    document.getElementById("arrow1").innerHTML = "  Body # {{$cow['id']}}";
        </script>
        @endsection
