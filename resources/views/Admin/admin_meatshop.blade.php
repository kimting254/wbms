@extends('app')

@section('content')
    <style>
        #arrow {
            font-weight: bold;
        }
        a.shadow:hover {

            box-shadow: 0 0 20px #A9A9A9;
        }
    </style>
    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 ">
                <div class="panel panel-{{Auth::user()->panels}}">
                    <div class="panel-heading">Available options for the <strong>Meatshop</strong>.</div>
                    <div class="panel-body">
                        <center>
                            <div class="pull-left">
                                <label class="label label-{{Auth::user()->labels}}" style="font-size:large;"> Receipts</label>
                            </div>
                            <div class="col-xs-10 col-sm-5 col-md-5 col-lg-5" style="margin-top:20px;">

                                <a  class = "shadow" href="./or">
                                    <img src="./assets/img/OR.png" style="width:150px;height: 100px;"/>
                                    <br>
                                    <label style="font-size: medium;color:black;"><strong>Official
                                            Receipts</strong></label>
                                </a>

                            </div>
                            <div class="col-xs-10 col-sm-5 col-md-5 col-lg-5" style="margin-top:20px;">
                                <a class = "shadow" href="./dr">
                                    <img src="./assets/img/DR.png" style="width:150px;height: 100px;"/>
                                    <br>
                                    <label style="font-size: medium; color:black;"><strong>Delivery
                                            Receipts</strong></label>
                                </a>

                            </div>
                            <div>
                                <div class="pull-left">
                                    <label class="label label-{{Auth::user()->labels}}" style="font-size:large;"> Resources</label>
                                </div>

                                <div class="col-xs-10 col-sm-3 col-md-3 col-lg-3" style="margin-top:50px; margin-left: 20px;">
                                    <a class = "shadow" href="./view_tabo_tabo">
                                        <img src="./assets/img/tabotabo.png" style="width:150px;height: 100px;"/>
                                        <br>
                                        <label style="font-size: medium;color:black;"><strong>Tabo-Tabo</strong></label>
                                    </a>
                                </div>
                                <div class="col-xs-10 col-sm-3 col-md-3  col-lg-3" style="margin-top:50px;">
                                    <a class = "shadow" href="./outsource">
                                        <img src="./assets/img/fromvendor.png" style="width:150px;height: 100px;"/>
                                        <br>
                                        <label style="font-size: medium;color:black;"><strong>From
                                                Vendors</strong></label>
                                    </a>
                                </div>
                                <div class="col-xs-10 col-sm-3 col-md-3  col-lg-3" style="margin-top:50px;">
                                    <a class = "shadow" href="./own">
                                        <img src="./assets/img/fromfarm.png" style="width:150px;height: 100px;"/>
                                        <br>
                                        <label style="font-size: medium;color:black;"><strong>From Farm</strong></label>
                                    </a>
                                </div>
                            </div>
                        </center>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>

                    </div>
                </div>
            </div>
        </div>
    </div>


    </div>
    <script>
        document.getElementById("arrow").innerHTML = "  Meatshop";
    </script>
@endsection
