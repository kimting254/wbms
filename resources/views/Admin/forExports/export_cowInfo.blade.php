<style>
    .table td {
        text-align: left;
        padding: 2px;
    }

    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        padding: 2px;
    }

</style>
<div class="container">
    <div class="row">
        <div class="col-lg-2 col-md-2 col-sm-2 col-lg-offset-1">
            <table class="table table-bordered col-lg-12 col-md-12 col-xs-12">
                <tbody>
                    <tr>
                        <td><b>Date Added : {{$cow['date']}}</b></td>
                        <td><b>Body Number : {{$cow['id']}}</b></td>
                        <td><b>Live Wt : {{$cow['lw']}}</b></td>
                        <td><b>Meatshop Wt : {{$cow['mw']}}</b></td>
                        <td><b>Slaughter Wt : {{$cow['sw']}}</b></td>
                    </tr>
                <tbody>
            </table>
        </div>

        <div class="col-lg-2 col-md-2 col-sm-2 col-lg-offset-1">
            <table class="table table-bordered col-lg-12 col-md-12 col-xs-12">
                <tbody>
                <tr>
                    <td><b>Carcass Parts( Beginning of Inventory From Slaughter)</b></td>
                </tr>
                <tr>
                    <td><b>Article <br> Name</b></td>
                    <td><b>Quantity <br>(Kls.)</b></td>
                </tr>
                @foreach($cow['main_parts'] as $part)
                    <tr>
                        <td>{{$part['article']}}</td>
                        <td>{{$part['quantity']}}</td>
                    </tr>
                @endforeach
                <tr style="border-top:solid 2px;">
                    <td><b>Total</b></td>
                    <td><b>{{$cow['mw']}} Kls</b></td>
                </tr>
                <tbody>
            </table>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-8">
            <table class="table table-bordered col-lg-12 col-md-12 col-xs-12">
                <thead style="border-bottom:solid 2px;">
                <th>Cut Types</th>
                <th>Article <br>Name</th>
                <th>Unit Price<br> (Php)</th>
                <th>Total <br>Qty</th>
                <th>Sales <br>Qty</th>
                <th>Remain <br>Qty</th>
                <th>Pack<br>()</th>
                <th>Sales <br>(Php)</th>
                <th>TOTAL <br>(Php)</th>
                </thead>
                <tbody>

                <tr>
                    <td><b>Choice Cuts</b></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <?php $total1 = 0; ?>
                @foreach($cow['article_resources'] as $list)
                    @if($list['type']=='Choice Cuts')
                        <tr>
                            <td></td>
                            <td>{{$list['article']['name']}} ({{$list['id']}})
                            </td>

                            <td>  @foreach($list['orderlist'] as $order)
                                    {{number_format($order['unit_price'],2)}}
                                    <br>
                                @endforeach

                            </td>
                            <td>
                                {{$list['quantity']}}


                            </td>

                            <td>  @foreach($list['orderlist'] as $order)
                                    {{$order['qty']}}
                                    <br>
                                @endforeach

                            </td>
                            <td>{{$list['remain_qty']}}</td>
                            <td>0</td>
                            <td>  @foreach($list['orderlist'] as $order)
                                    {{number_format($order['amount'],2)}}
                                    <?php $total1 = $total1 + $order['amount'] ?>
                                    <br>
                                @endforeach

                            </td>
                            <td></td>
                        </tr>
                    @endif
                @endforeach
                <tr style="border-top:solid 2px;">
                    <td></td>
                    <td></td>
                    <td></td>

                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><b>{{number_format($total1,2)}}</b></td>
                </tr>

                <tr>
                    <td><b>Special Cuts</b></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <?php $total2 = 0; ?>
                @foreach($cow['article_resources'] as $list)
                    @if($list['type']=='Special Cuts')
                        <tr>
                            <td></td>
                            <td>{{$list['article']['name']}} ({{$list['id']}})
                            </td>

                            <td>  @foreach($list['orderlist'] as $order)
                                    {{number_format($order['unit_price'],2)}}
                                    <br>
                                @endforeach

                            </td>
                            <td>
                                {{$list['quantity']}}


                            </td>

                            <td>  @foreach($list['orderlist'] as $order)
                                    {{$order['qty']}}
                                    <br>
                                @endforeach

                            </td>
                            <td>{{$list['remain_qty']}}</td>
                            <td>0</td>
                            <td>  @foreach($list['orderlist'] as $order)
                                    {{number_format($order['amount'],2)}}
                                    <?php $total2 = $total2 + $order['amount'] ?>
                                    <br>
                                @endforeach

                            </td>
                        </tr>
                    @endif
                @endforeach
                <tr style="border-top:solid 2px;">
                    <td></td>
                    <td></td>
                    <td></td>

                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><b>{{number_format($total2,2)}}</b></td>
                </tr>


                <tr>
                    <td><b>Other Cuts</b></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <?php $total3 = 0; ?>
                @foreach($cow['article_resources'] as $list)
                    @if($list['type']=='Other Cuts')
                        <tr>
                            <td></td>
                            <td>{{$list['article']['name']}} ({{$list['id']}})
                            </td>

                            <td>  @foreach($list['orderlist'] as $order)
                                    {{number_format($order['unit_price'],2)}}
                                    <br>
                                @endforeach

                            </td>
                            <td>
                                {{$list['quantity']}}


                            </td>

                            <td>  @foreach($list['orderlist'] as $order)
                                    {{$order['qty']}}
                                    <br>
                                @endforeach

                            </td>
                            <td>{{$list['remain_qty']}}</td>
                            <td>0</td>
                            <td>  @foreach($list['orderlist'] as $order)
                                    {{number_format($order['amount'],2)}}
                                    <?php $total3 = $total3 + $order['amount'] ?>
                                    <br>
                                @endforeach

                            </td>
                        </tr>
                    @endif
                @endforeach
                <tr style="border-top:solid 2px;">
                    <td></td>
                    <td></td>
                    <td></td>

                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><b>{{number_format($total3,2)}}</b></td>
                </tr>


                <tr>
                    <?php $total4 = $total1 + $total2 + $total3 ?>
                    <td><b>TOTAL SALES</b></td>
                    <td></td>
                    <td></td>

                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><b>{{number_format($total4,2)}}</b></td>
                </tr>
                <tbody>
            </table>
        </div>
    </div>
</div>