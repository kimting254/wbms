<div class="col-xs-6 col-xs-12">
    <label>Year :</label>
    <select name="year" class="form-control">
        @foreach($year1 as $data)
            <option>{{$data}}</option>
        @endforeach
    </select>
</div>
<div class="col-xs-6 col-xs-12">
    <label>Month :</label>
    <select name="month" class="form-control">
        @foreach($month1 as $data)
            <option>{{$data}}</option>
        @endforeach
    </select>
</div>