<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleResourcesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article__resources', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cow_id')->nullable();
            $table->integer('outsource_id')->nullable();
            $table->integer('tabo_tabo_id')->nullable();
            $table->string('quantity');
            $table->integer('article_id');
            $table->integer('total_article_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('article__resources');
    }

}
