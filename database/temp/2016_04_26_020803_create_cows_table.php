<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCowsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('cows', function(Blueprint $table) {
            $table->increments('id');
            $table->string('lw');
            $table->integer('body_number');
            $table->string('sw');
            $table->string('mw');
            $table->string('sex');
            $table->string('status');
            $table->string('source');
            $table->string('area');
            $table->string('owner');
            $table->string('color');
            $table->string('price');
            $table->string('price_per_kilo');
            $table->string('gross');
            $table->string('expense');
            $table->string('date_of_purchase');
            $table->string('income/deff');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('cows');
    }

}
