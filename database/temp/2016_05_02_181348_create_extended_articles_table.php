<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExtendedArticlesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('extended_articles', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('total_article_id');
			$table->double('quantity');
			$table->string('extended_article_id');
			$table->timestamps();
		});
	}

	/**
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('extended_articles');
	}

}
