<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaboTabosTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabo_tabos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lw');
            $table->string('sw');
            $table->string('mw');
            $table->string('sex');
            $table->string('area');
            $table->string('owner');
            $table->string('color');
            $table->string('price');
            $table->string('price_per_kilo');
            $table->string('gross');
            $table->string('expense');
            $table->string('year');
            $table->string('month');
            $table->string('date_of_purchase');
            $table->string('income_deff');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tabo_tabos');
    }

}
